<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Juniper Shop</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="images/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
   </head>
   <!-- body -->
   <body class="main-layout">
      <!-- loader  -->
      <!-- end loader --> 
      <!-- header -->
      <header>
         <!-- header inner -->
         <div class="container">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                  <div class="full">
                     <div class="center-desk">
                        <div class="logo"> <a href="index.php"><img src="images/logoo.jpg" alt="logo"/></a> </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-7 col-lg-7 col-md-9 col-sm-9">
                  <div class="menu-area">
                     <div class="limit-box">
                        <nav class="main-menu">
                           <ul class="menu-area-main">
                              <li class="active"> <a href="index.php">Home</a> </li>
                              <li> <a href="#about">About</a> </li>
                              <li> <a href="#product">product</a> </li>
                              <li> <a href="#contact"> Contact</a> </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
               <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                  <li><a class="buy" href="logout.php">Logout</a></li>
               </div>
            </div>
         </div>
         <!-- end header inner --> 
      </header>
      <!-- end header -->
      <section class="slider_section">
         <div id="main_slider" class="carousel slide banner-main" data-ride="carousel">

            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img class="first-slide" src="images/banner2.jpg" alt="First slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Produk <br> <strong class="black_bold">Terbaru </strong><br>
                           <strong class="yellow_bold">Kami </strong></h1>
                        <p>Tas Pria Yang Menggabungkan Gaya dan Fungsi <br>
                          Kualitas Yang Terbaik dan Harga Yang Terbaik </p>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="second-slide" src="images/banner2.jpg" alt="Second slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Produk <br> <strong class="black_bold">Terbaru</strong><br>
                           <strong class="yellow_bold">Kami </strong></h1>
                        <p>Temukan Tas Pria Elegan dan Berkelas <br>
                          Yang Siap Menemani Anda Dengan Gaya dan Kenyamanan </p>
                     </div>
                  </div>
               </div>

            </div>
            <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
            <i class='fa fa-angle-right'></i>
            </a>
            <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
            <i class='fa fa-angle-left'></i>
            </a>
            
         </div>

      </section>


<div class="about" id="about">
   <div class="container">
      <div class="row">
         <dir class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
            <div class="about_box">
               <figure><img src="images/bag.png"/></figure>
            </div>
         </dir>
          <dir class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
            <div class="about_box">
               <h3>Temukan Gayamu</h3>
               <p>Dengan koleksi kami yang kaya akan gaya, kualitas, dan kepraktisan, kami memastikan bahwa setiap tas yang Anda pilih akan menjadi teman setia dalam segala petualangan Anda. Dari tas selempang yang elegan hingga ransel yang tangguh, setiap detail dirancang dengan cermat untuk memenuhi kebutuhan dan ekspektasi Anda.</p>
               <p>Kami mengutamakan kualitas terbaik dengan bahan yang tahan lama dan proses pembuatan yang teliti. Setiap jahitan dan aksen diperhatikan secara detail untuk memastikan produk-produk kami tahan lama dan memikat.

Selain itu, staf berpengetahuan luas kami siap membantu Anda menemukan tas pria yang sempurna sesuai dengan gaya dan preferensi Anda.</p>
            </div>
         </dir> 
      </div>
   </div>
<!-- CHOOSE  -->
      <div class="whyschose">
         <div class="container">

            <div class="row">
               <div class="col-md-7 offset-md-3">
                  <div class="title">
                     <h2>Kenapa <strong class="black">Memilih Kami</strong></h2>
                     <span>Fastest repair service with best price!</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="choose_bg">
         <div class="container">
            <div class="white_bg">
            <div class="row">
               <dir class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="for_box">
                     <i><img src="icon/1.jpg"/></i>
                     <h3>Fungsi & Kenyamanan</h3>
                     <p>Bukan hanya tampil menarik, tapi produk kami juga mengutamakan fungsi</p>
                  </div>
               </dir>
               <dir class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="for_box">
                     <i><img src="icon/2.jpg"/></i>
                     <h3>Kualitas Yang Unggul</h3>
                     <p>Menawarkan produk dengan kualitas tinggi, sehingga tahan lama</p>
                  </div>
               </dir>
               <dir class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="for_box">
                     <i><img src="icon/3.jpg"/></i>
                     <h3>Desain Yang Menarik</h3>
                     <p>Memiliki banyak pilihan yang stylish dan trend terkini</p>
                  </div>
               </dir>
               <dir class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="for_box">
                     <i><img src="icon/4.jpg"/></i>
                     <h3>Pelayanan Yang Terbaik</h3>
                     <p>Berkomitmen memberikan pelayanan terbaik terhadap pelanggan</p>
                  </div>
               </dir>
               
            </div>
         </div>
       </div>
      </div>
</div>
<!-- end CHOOSE -->

<style>
.button-orange {
  background-color: orange;
  color: white;
  width: 100px;
  height: 40px;
  border: none;
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border-radius: 4px;
  transition: background-color 0.3s;
}

.button-orange:hover {
  background-color: rgb(255, 136, 0);
}
</style>

      <!-- our product -->
      <div class="product" id="product">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="title">
                     <h2>Produk <strong class="black">Kami</strong></h2>
                     <span>Menjamin kualitas dan harga terbaik baik pelanggan.</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="product-bg">
         <div class="product-bg-white">
         <div class="container">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p1.jpg"/></i>
                     <h3>Antrophologie Bag</h3>
                     <span>Rp 250.000</span><br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p2.jpg"/></i>
                     <h3>Backpacks Burton</h3>
                     <span>Rp 300.000</span><br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p3.jpg"/></i>
                     <h3>yellow Bag</h3>
                     <span>Rp 350.000</span>
                     <br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p4.jpg"/></i>
                     <h3>Mochilas Bag Casual</h3>
                     <span>Rp 250.000</span>
                     <br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p5.jpg"/></i>
                     <h3>Amazon Bag</h3>
                     <span>Rp 400.000/span><br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p8.jpg"/></i>
                     <h3>Burton Pack</h3>
                     <span>Rp 350.000</span><br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p6.jpg"/></i>
                     <h3>Duffle Bag</h3>
                     <span>220.000</span><br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="product-box">
                     <i><img src="icon/p7.jpg"/></i>
                     <h3>Urban Zipper bag</h3>
                     <span>Rp 450.000</span><br>
                     <button class="button-orange">Beli</button>
                  </div>
               </div>
              
              
              
               
               </div>
            </div>
         </div>
         
        
        
      </div>

            </div>
         </div>

         <div class="container" id="contact">
            <div class="yellow_bg">
            <div class="row">
               <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12">
                  <div class="yellow-box">
                     <h3>Kontak Kami<i><img src="icon/calll.png"/></i></h3>
                     
                     <p>+62 822 3189 4868</p>
                     <p>JO@gamil.com</p>
                  </div>
               </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                  <div class="yellow-box">
                     <a href="#">Kirim Pesan</a>
                  </div>
               </div>
            </div>
         </div>
         </div>
      </div>

      <!-- end our product -->

      <!--  footer --> 




      <footr>
         <div class="footer">
            <div class="container">
               <div class="row">
                  <div class="col-md-6 offset-md-3">
                     <ul class="sociel">
                         <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                         <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                         <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                         <li> <a href="#"><i class="fa fa-facebook"></i></a></li>
                     </ul>
                  </div>
            </div>
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="contact">
                     <h3>Contact</h3>
                     <span>Medan<br>
                       <br>
                        +62 822 3189 4868</span>
                  </div>
               </div>
                 <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="contact">
                     <h3>Daftar Menu</h3>
                     <ul class="lik">
                         <li> <a href="index.php">Home</a></li>
                         <li> <a href="#about">About</a></li>
                         <li> <a href="#product">Product</a></li>
                         <li> <a href="#contact">Contact</a></li>
                          <li> <a href="logout.php">Logout</a></li>
                     </ul>
                  </div>
               </div>
                 
                 <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                  <div class="contact">
                     <h3>50% OFF</h3>
                     <span>Untuk pelanggan yang menjadi Member </span>
                  </div>
               </div>
            </div>
         </div>
            <div class="copyright">
               <p>Copyright JO Shop 2023</p>
            </div>
         
      </div>
      </footr>
      <!-- end footer -->
      <!-- Javascript files--> 
      <script src="js/jquery.min.js"></script> 
      <script src="js/popper.min.js"></script> 
      <script src="js/bootstrap.bundle.min.js"></script> 
      <script src="js/jquery-3.0.0.min.js"></script> 
      <script src="js/plugin.js"></script> 
      <!-- sidebar --> 
      <script src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
      <script src="js/custom.js"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
         openEffect: "none",
         closeEffect: "none"
         });
         
         $(".zoom").hover(function(){
         
         $(this).addClass('transition');
         }, function(){
         
         $(this).removeClass('transition');
         });
         });
         
      </script> 
   </body>
</html>